import React, { Component } from 'react';
import { Figure, Form, Card, Navbar, Button, Row, Col, Container } from 'react-bootstrap';
import NavHeader from './components/NavHeader';
import TableList from './components/TableList'



export default class App extends Component {
  constructor(){
    super();
    this.state = {
      data: [
        {
          id: 1,
          username: "Chhinghor Leng",
          email: "lengchhinghor@gmail.com",
          password: "123456",
          gender: "Male",
          isDeleted:false
        },
        {
          id: 2,
          username: "Kai Keo",
          email: "kaikeo@gmail.com",
          password: "123456",
          gender: "Male",
          isDeleted:false
        },
        {
          id: 3,
          username: "That Chhaihong",
          email: "Chhaihongthat@gmail.com",
          password: "123456",
          gender: "Male",
          isDeleted:false
        },
      ],

      username: '',
      gender:'',
      email: ''

    }
    this.getBeforeDelete = this.getBeforeDelete.bind(this)
    this.onDelete = this.onDelete.bind(this)

  }


  handleUsername = event => {
    this.setState({
      data: event.target.value
    })
  }

  handleSubmit = event => {
    console.log(this.state.username)
    console.log(this.state.gender)
    let data = {
          id: this.state.data.length+1,
          username: this.state.username,
          email:this.state.email,
          password: this.state.password,
          gender: this.state.gender,
          isDeleted:false
    }
    let getData = [...this.state.data, data]
    this.setState({
      data: getData
    })
    this.setState({username: ""}) ;
  }
  getBeforeDelete=(index)=>{
    console.log(index)
    let data = [...this.state.data]
    data[index].isDeleted= !data[index].isDeleted
    this.setState({
      data:data
    })
    console.log(data[index])
  }
  onDelete =()=>{
    let data = [...this.state.data]
    let newData=[]
    console.log(data)
    for(let i = 0; i < data.length; i++){
      console.log(i)
      if(!data[i].isDeleted){
        newData.push(data[i])
      }
    }
    this.setState({
      data:newData
      // data:data
    })
  }
  render() {
    return (
     <Container>
        <Row>
            <Col md={12}>
            <NavHeader />
            </Col>
            <Col md={4}>
            <Figure>
            <Figure.Image
              className=" d-block mx-auto img-fluid w-50"
              width={171}
              height={180}
              alt="171x180"
              src="image/avatar.png"
            />
            <Card.Title><h3 className="text-center">Create Account</h3></Card.Title>
            </Figure>
            <Form >
              <Form.Group controlId="formUserName">
                <Form.Label>Username</Form.Label>
                <Form.Control value={this.state.username} onChange={(e) => this.setState({ username: e.target.value })} type="text"  placeholder="Enter username" />
              </Form.Group>

              <Form.Group controlId="formRadio">
                <Form.Label>Gender</Form.Label>
                {['radio'].map((type) => (
                <div key={`inline-${type}`} className="mb-3">
                  <Form.Check inline label="Male" value={this.state.gender} onChange={(e) => this.setState({ gender:"Male" })}  name="group1" type={type} id={`inline-${type}-male`} />
                  <Form.Check inline label="Female" value={this.state.gender} onChange={(e) => this.setState({ gender:"Female" })} name="group1" type={type} id={`inline-${type}-female`} />
                </div>
              ))}
              </Form.Group>
              
              <Form.Group controlId="formBasicEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control value={this.state.data.email} onChange={(e) => this.setState({ email: e.target.value })} type="email"  placeholder="Enter email" />
              </Form.Group>

              <Form.Group controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control value={this.state.data.password} type="password" placeholder="Password" />
              </Form.Group>
              <Button variant="primary" onClick={()=>this.handleSubmit()}>
                Save
              </Button>
            </Form>
            </Col>
            <TableList ondelete={this.onDelete} onDeleteMethod={this.getBeforeDelete} items={this.state.data} />
        </Row>
     </Container>
    )
  }
}

