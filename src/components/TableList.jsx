import React from 'react';
import { Container, Button, Table,Badge,Row, Col } from 'react-bootstrap'


function TableList(props) {
    // console.log(props.items);
    return (
        
            <Col md={8}>
                <Container>
                <h3 className="my-5 ">Table Account</h3>
                
                <Table striped bordered hover>
                <thead>
                    <tr>
                    <th>#</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Gender</th>
                    </tr>
                </thead>
                <tbody>
                
                {
                   props.items.map((item,idx)=>
                     <tr key={idx} onClick={()=>props.onDeleteMethod(idx)}style={{color:item.isDeleted?"red":"blue"}}>
                     <td>{item.id}</td>
                     <td>{item.username}</td>
                     <td>{item.email}</td>
                     <td>{item.gender}</td>
                     </tr>
                )
                }
                {/* <tr>
                     <td>1</td>
                     <td>lengchhinghor</td>
                     <td>lengchhinghor@gmail.com</td>
                     <td>male</td>
                     </tr> */}
                     
                    
                </tbody>
               
            </Table>
            <Button variant="danger" onClick={()=>props.ondelete()}>
                Delete
              </Button>
                </Container>
            
            </Col>
    )
}
export default TableList

